using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class ClockController : MonoBehaviour
{
    public Text textTimee24Hr;
    public Text textTimeAMPM;

    void Update()
    {
        TimeNow();
    }

    public void TimeNow()
    {
        textTimee24Hr.text = DateTime.Now.ToString("HH:mm:ss");

        textTimeAMPM.text = DateTime.Now.ToString("hh:mm:ss tt");
    }
}
