using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBtn : MonoBehaviour
{
    public GameObject objectRotate;

    public float rotateSpeed = 30f;
    bool rotateStatus = false;

    void Update()
    {
        if (rotateStatus == true)
        {
            objectRotate.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
        }
    }

    public void RotateObj()
    {
        if (rotateStatus == false)
        {
            rotateStatus = true;
        }
        else
        {
            rotateStatus = false;
        }
    }
}
