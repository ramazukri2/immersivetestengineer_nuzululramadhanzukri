using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ScaleHoldBtn : MonoBehaviour
{
    public bool isHold;
    public float multipleScale;
    public float maxScale, minScale;
    public GameObject targetScale;
    Vector3 defaultScaleObject;

    public Button[] buttonScale;

    void Update()
    {
        if (isHold == true)
        {
            IncreaseScaleObject();
            DecreaseScaleObject();
        }
    }

   

    public void IncreaseScaleObject()
    {
        if (targetScale.transform.localScale.x < maxScale)
        {
            targetScale.transform.localScale = new Vector3(targetScale.transform.localScale.x + multipleScale, targetScale.transform.localScale.y + multipleScale, targetScale.transform.localScale.z +  multipleScale);
        }
        else
        {
            buttonScale[0].interactable = false;
            buttonScale[1].interactable = true;
        }
    }
    public void DecreaseScaleObject()
    {
        if (targetScale.transform.localScale.x > minScale)
        {
            targetScale.transform.localScale = new Vector3(targetScale.transform.localScale.x - multipleScale, targetScale.transform.localScale.y - multipleScale, targetScale.transform.localScale.z - multipleScale);
        }
        else
        {
            buttonScale[1].interactable = false;
            buttonScale[0].interactable = true;
        }
    }

    public void TargetFound(GameObject target)
    {

        target.transform.localScale = new Vector3(100, 100, 100);
    }

    public void TargetLoss(GameObject target)
    {
        target.transform.localScale = new Vector3(0, 0, 0);
        buttonScale[0].interactable = true;
        buttonScale[1].interactable = true;
    }
}
