using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class PreviewPhoto : MonoBehaviour
{
    public GameObject theQuad;
    public Camera thePreviewCam;
    public TakePhoto Takefoto;

    public void ShareButton()
    {
        StartCoroutine(TakeSSAndShare());
    }

    public void GallerySave()
    {
        StartCoroutine(SaveGallery());
    }
    //{
    //    Sprite sp = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 1000.0f);
    //   Takefoto.previewPhoto.sprite = sp;
    //}

    public IEnumerator RecordFoto()
    {
        yield return new WaitForEndOfFrame();
        var texture = ScreenCapture.CaptureScreenshotAsTexture();

        Takefoto.previewPhoto.sprite = SpriteFromTexture2D(texture);

    }

    private IEnumerator TakeSSAndShare()
    {
        yield return new WaitForEndOfFrame();

        Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        ss.Apply();

        string filepath = Path.Combine(Application.temporaryCachePath, "sharedimg.png");
        File.WriteAllBytes(filepath, ss.EncodeToPNG());

        Destroy(ss);

        new NativeShare().AddFile(filepath).SetSubject("AR Share").SetText("Share Foto").Share();
    }

    private IEnumerator SaveGallery()
    {
        yield return new WaitForEndOfFrame();
        Texture2D texture = ScreenCapture.CaptureScreenshotAsTexture();

        string name = "Screenshot_Result" + System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".png";

        //pc
        //byte[] bytes = texture.EncodeToPNG();
        //File.WriteAllBytes(Application.dataPath + "/PhotoResult/" + name, bytes);

        //mobile
        NativeGallery.SaveImageToGallery(texture, "ARPhoto", name);
    }

    Sprite SpriteFromTexture2D(Texture2D texture)
    {
        return Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.0f, 0.0f), 1000.0f);
    }

    public void CloseBtn()
    {
        theQuad.SetActive(false);
        Takefoto.CountdownTimer.SetActive(true);
        Takefoto.countdownTime = 3;

    }

    public void RetakeBtn()
    {
        Takefoto.CountdownTimer.SetActive(true);
        Takefoto.countdownTime = 3;
        theQuad.SetActive(false);


        //LoopingSystem();
    }

    public void DeleteBtn()
    {
        theQuad.SetActive(false);
        Takefoto.CountdownTimer.SetActive(true);
        Takefoto.countdownTime = 3;
    }
}
