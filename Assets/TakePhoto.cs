using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class TakePhoto : MonoBehaviour
{
    public GameObject UI;
    public GameObject CountdownTimer;

    public Image previewPhoto;
    public PreviewPhoto preview;
    public int countdownTime;


    public float currentTime = 0f;
    //public float startingTime = 3f;

    public Text countdownText;

    void Start()
    {
        ShowPreviewPhoto(false);

    }

    void Update()
    {

    }


    public void TakeScreenShot()
    {
        StartCoroutine("Screenshot");
        UI.SetActive(false);
    }

    public void TimerPhoto()
    {

        if (countdownTime < 0)
        {
            countdownText.gameObject.SetActive(false);
            //ShowPreviewPhoto(true);
            //previewPhoto.gameObject.SetActive(true);

        }

        //currentTime = startingTime;
        //currentTime -= 1 * Time.deltaTime;
        //countdownText.text = currentTime.ToString("0");
    }

    private void CaptureScreenshoot(Sprite sprite)
    {
        if (sprite != null)
        {
            previewPhoto.sprite = sprite;
            ShowPreviewPhoto(true);
        }
    }

    public void ClosePreview()
    {
        ShowPreviewPhoto(false);
    }

    private void ShowPreviewPhoto(bool visible)
    {
        previewPhoto.gameObject.SetActive(visible);
        TimerPhoto();
    }


    private IEnumerator Screenshot()
    {
        yield return CountdownToStart();
        yield return new WaitForEndOfFrame();
        Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);

        texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        texture.Apply();

        Destroy(texture);
        UI.SetActive(true);
        ShowPreviewPhoto(true);

    }

    public IEnumerator CountdownToStart()
    {

        while (countdownTime > 0)
        {
            countdownText.text = countdownTime.ToString();
            yield return new WaitForSeconds(1f);
            countdownTime--;
        }

        CountdownTimer.SetActive(false);

        yield return preview.RecordFoto();
    }

}
